<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator_model extends CI_Model {

	
	function admin_signin($admin_email, $admin_password)
	{
		$password_admin	= hash("MD5", $admin_password);
		$sql	= "SELECT * FROM admin
					WHERE
						admin_email = '$admin_email'
					AND
						admin_password = '$password_admin'";
		$query 	= $this->db->query($sql);
		$result	= $query->row();
		return $result;
	}
	
	
	function getPesan()
	{
		$this->db->select('*');
		$this->db->from('kontak');
		$query 	= $this->db->get();
		$result	= $query->result();
		return $result;
	}
	
	function getPasien($where = ''){
			return $this->db->query("select * from pasien $where; ");
		}
		
	function getPasien0()
	{
		return $this->db->query("select * from pasien where progress = '0'; ");	
	}
	
	function getPasien1()
	{
		return $this->db->query("select * from pasien where progress = '1'; ");	
	}
		
	function getSpesialis($where = ''){
			return $this->db->query("select * from spesialis $where; ");
		}
		
	function getDokter($where = ''){
			return $this->db->query("select * from dokter $where; ");
		}
	
	function spesialis(){
		$this->db->select('*');
		$this->db->from('spesialis');
		$this->db->where('aktif',1);
		$query 	= $this->db->get();
		$result	= $query->result();
		return $result;
		}
		
	
	function dokter(){
		$this->db->select('*');
		$this->db->from('dokter');
		$query 	= $this->db->get();
		$result	= $query->result();
		return $result;
		}
		
	function kontak_kirim($data){
		$this->db->insert('kontak',$data);
		}
		
	
	function pasien_delete_info($pasien_id)
	{
		$this->db->where('pasien_id',$pasien_id);
		$this->db->delete('pasien');
	}
	
	function insertdata($tabel, $data)
	{
		return $this->db->insert($tabel, $data);
	}
	
	function updatedata($tabel, $data, $where)
	{
		return $this->db->update($tabel, $data, $where);	
	}
	

	function dokter_delete_info($dokter_id)
	{
		$this->db->where('dokter_id',$dokter_id);
		$this->db->delete('dokter');
	}
	
	function spesialis_delete_info($spesialis_id)
	{
		$this->db->where('spesialis_id',$spesialis_id);
		$this->db->delete('spesialis');
	}
	
	function pesan_delete_info($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('kontak');
	}
	
	

}


