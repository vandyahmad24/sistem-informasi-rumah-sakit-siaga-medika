    
      <!-- Department -->
      <div class="department">
        <div class="container"> 
          <!-- Tab Panel -->
          <div role="tabpanel">
         
            
            <!-- Tab Content -->
            <div class="tab-content"> 
              
              <!-- Sambutan -->
              <div role="tabpanel" class="tab-pane fade in active" id="cardiology"> 
                <!-- Depatment Text Section -->
                <div class="dep-sec-txt">
                  <div class="row">
                    <div class="col-md-7">
                      <h5>Sambutan Kepala Klinik</h5>
                      <p> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, <br>
                        <br>
                        It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s PageMaker including versions of Lorem Ipsum. </p>
                      
                      <!-- List Style Check -->
                      <ul class="list-style-check">
                        <li> Gawat Darurat </li>
                        <li> Dokter Terkualifikasi </li>
                        <li> 24x7 Pelayanan Darurat </li>
                        <li> Bebas Konsultasi Kesehatan </li>
                        
                      </ul>
                    </div>
                    
                    <!-- Doctor Img -->
                    <div class="col-md-offset-1 col-md-4">
                      <div class="doctor-img"> <img class="img-responsive" src="<?php echo base_url(); ?>/assets/web/images/dokter.jpg" alt="" >
                        <div class="name-doc text-center">
                          <h6>Drs. Nurul Kamalia <br>Direktur Klinik Siaga Medika Purwokerto</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  

      <!-- DOCTOR LIST -->
    <section class="p-t-b-150">
      <div class="container"> 
        
        <!-- Heading -->
        <div class="heading-block">
             <h2>Layanan & Fasilitas</h2>
          <hr>
        
        
        <!-- Services -->
        <div class="services">
          <div class="row"> 
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-eye-2 icon"></i> </div>
                <div class="media-body">
                  <h6>Spesialis Mata</h6>
                  <p>Periksa Mata Anda Dengan teknologi terkomputasi</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-operating-room icon"></i> </div>
                <div class="media-body">
                  <h6>Ruang Operasi</h6>
                  <p>Ruang operasi dengan teknologi terbaru dan steril.</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-icu-monitor icon"></i> </div>
                <div class="media-body">
                  <h6> RUANG ICU </h6>
                  <p>Penanggan pasien dengan cepat dan perawatan yang ramah</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-doctor icon"></i> </div>
                <div class="media-body">
                  <h6> Dokter Terkualifikasi</h6>
                  <p>Anda atau Keluarga anda akan diserahkan pada dokter yang ahli dibidangnya</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-heartbeat icon"></i> </div>
                <div class="media-body">
                  <h6>Serangan Jantung Problems</h6>
                  <p>Perawatan pasien serangan jantung dengan alat berteknologi tinggi</p>
                </div>
              </article>
            </div>
            
            <!-- Services -->
            <div class="col-md-4">
              <article>
                <div class="media-left"> <i class="flaticon-stomach-2 icon"></i> </div>
                <div class="media-body">
                  <h6>Penyakit Lambung </h6>
                  <p>Menangani penyakit lambung anda </p>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    </section>
    
  <!-- Fun Fact -->
    <section class="fun-fact" data-stellar-background-ratio="0.5">
      <div class="container"> 
        
        <!-- Counter -->
        <div class="counters nolist-style">
          <ul class="row">
            
            <!-- Satisfied Clients -->
            <li class="col-sm-3"> 
              
              <!-- icon -->
              <div class="media-left"> <i class="fa fa-smile-o"> </i> </div>
              <div class="media-body"> <span class="counter" ><?= $pasien_total ?></span>
                <p>Pasien</p>
              </div>
            </li>
            
            <!-- PROPOSALS SENT -->
            <li class="col-sm-3">
              <div class="media-left"> <i class="flaticon-doctor icon"> </i> </div>
              <div class="media-body"> <span class="counter"><?= $spesialis_total ?></span>
                <p>Spesialis</p>
              </div>
            </li>
            
            <!-- AWARDS WON -->
            <li class="col-sm-3">
              <div class="media-left"> <i class="fa fa-user-md"> </i> </div>
              <div class="media-body"> <span class="counter"><?= $dokter_total ?></span>
                <p>Dokter</p>
              </div>
            </li>
            <li class="col-sm-3">
              <div class="media-left"> <i class="fa fa-ambulance"> </i> </div>
              <div class="media-body"> <span>24/7</span>
                <p>Ambulan Non Stop</p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </section>
   <?php

  include ('token.php'); 
?>