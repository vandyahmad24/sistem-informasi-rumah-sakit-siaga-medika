<!-- Bnr Header -->
  
  
 <!-- What We Do -->
    <section class="p-t-b-150 padding-bottom-0">
      <div class="row light-gry-bg"> 
        
        <!-- What We Do -->
        <div class="col-lg-6">
          <div class="inside-sapce"> 
            
            <!-- Heading -->
            <div class="heading-block head-left margin-bottom-50">
              <h2>Temukan Kami Di</h2>
              <hr>
               
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.3248067733584!2d109.24570281462904!3d-7.429262594640069!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e655e9ad68fe343%3A0xd03e4f429baa5864!2sStandby+Medika+Psychiatric+Hospital!5e0!3m2!1sen!2sid!4v1565354821683!5m2!1sen!2sid" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
         
         <br /><br />    
            <!-- BOXES -->
    <section class="contact-box">
      <div class="container">
        <div class="row"> 
          
          <!-- Shop Location -->
          <div class="col-md-7">
            <div class="boxes-in">
              
              <ul class="location">
                <li> <i class="fa fa-location-arrow"></i>
                 <?php foreach ($alamat as $k) { ?>
            <p><?php echo $k->content; ?></p>
            <?php } ?>
                </li>
                <li> <i class="fa fa-phone"></i>
                  <?php foreach ($kontak as $k) { ?>
                <p><?php echo $k->content; ?></p>
                <?php } ?>
                </li>
              
              </ul>
            </div>
          </div>
          
          
        </div>
      </div>
    </section> 
             
             
          </div>
        </div>
        <?php 
                $message = $this->session->userdata('message');
                if($message){
                  echo "<p>".$message."</p>";
                  $message = $this->session->unset_userdata('message');
                }
              ?>
        
        <!-- Make an Appointment -->
        <div class="col-lg-6">
          <div class="inside-sapce appointment bg"> 
            
            <!-- Heading -->
            <div class="heading-block head-left margin-bottom-50 white">
              <h2>Tinggalkan Pesan</h2>
              <hr>
              <span>Silahkan isi formulir berikut untuk mengirim komentar, pertanyaan, dan lainnya.</span> </div>
              
              <span>
             
              </span>
            <form class="appointment-form" action="<?= base_url();?>index.php/Controller_web/kontak_kirim" method="POST">
              <ul class="row">
                <li class="col-sm-6">
                  <label>
                    <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Anda" required>
                    <i class="icon-user"></i> </label>
                </li>
                <li class="col-sm-6">
                  <label>
                    <input type="email" id="email" name="email" class="form-control" placeholder="Alamat Email" required>
                    <i class="icon-envelope"></i> </label>
                </li>
                <li class="col-sm-6">
                  <label>
                    <input type="text" id="hp" name="hp" class="form-control" placeholder="Nomor Kontak" required>
                    <i class="icon-call-in"></i> </label>
                </li>
                <li class="col-sm-6">
                  <label>
                    <select class="selectpicker" id="sex" name="sex">
                      <option>Jenis Kelamin</option>
                      <option>Laki-Laki</option>
                      <option>Perempuan</option>
                    </select>
                    <i class="icon-direction"></i></label>
                </li>
                
                <li class="col-sm-12">
                  <label>
                    <textarea class="form-control" id="message" name="message" placeholder="Masukan Pesan atau Komentar Anda Disini"></textarea>
                  </label>
                </li>
                <li class="col-sm-12">
                  <button type="submit" class="btn">Kirim Pesan atau Komentar</button>
                </li>
              </ul>
            </form>
          </div>
        </div>
      </div>
    </section>

     <?php

  include ('token.php'); 
?>