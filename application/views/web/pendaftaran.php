<!-- Make an Appointment -->
    <section class="main-oppoiment">
      <div class="container">
        <div class="row"> 
          
          <!-- Make an Appointment -->
          <div class="col-lg-7">
            <div class="appointment"> 
               
              <!-- Heading -->
              <div class="heading-block head-left margin-bottom-50">
                <h4>Form Pendaftaran Pasien</h4>
                <br><br>
                <span>Silahkan isi formulir berikut ini untuk melakukan pendaftaran pemeriksaan pasien. Setelah anda mendaftarkan diri anda. Anda akan mendapatkan nomor token yang dapat digunakan untuk melihat rekam medis, pemeriksaan oleh dokter dimanapun anda berada.</span> </div>

                  <?php
				$message	= $this->session->userdata('message');
				$token		= $this->session->userdata('token');
					if(!empty($message)){
						echo "<h4>".$message."</h4>";
						echo "Silahkan catat nomor Token Anda untuk melihat hasil pemeriksaan dari dokter.";
						echo "<h2>Token Anda: ".$token."</h2>";
						$message	= $this->session->unset_userdata('message');
						$token		= $this->session->unset_userdata('token');
						}
					else {
				
             	?>

              <form class="appointment-form" action="<?= base_url();?>index.php/Controller_web/pendaftaran_save" method="post">
                <ul class="row">
                	 <li class="col-sm-6">
                      <input type="hidden" class="form-control" id="perawatan_id" name="perawatan_id" value="<?php echo date("ymdhs",time()); ?>" >
                    <label>
                      <input type="text" class="form-control" id="nama_pasien" name="nama_pasien" placeholder="Nama Anda" required>
                      <i class="icon-user"></i> </label>
                  </li>
                  <li class="col-sm-6">
                    <label>
                      <input type="email" class="form-control" id="email_pasien" name="email_pasien" placeholder="Alamat Email Anda" required>
                      <i class="icon-envelope"></i> </label>
                  </li>
                  <li class="col-sm-6">
                    <label>
                      <input type="text" class="form-control" name="kontak" id="kontak" placeholder="Nomor Telepon Anda" required>
                      <i class="icon-call-in"></i> </label>
                  </li>
                  <li class="col-sm-6">
                    <label>
                      <select class="selectpicker" name="sex" id="sex">
                      	<option>Pilih Jenis Kelamin</option>
                        <option>Pria</option>
                        <option>Wanita</option>
                      </select>
                      <i class="icon-direction"></i></label>
                  </li>
                  <li class="col-sm-6">
                    <label>
                      <select class="selectpicker" id="spesialis_id" name="spesialis_id">
                        <option>Pilih Spesialis</option>
                        <?php foreach($nama_spesialis as $s) { ?> 
                        	<option value="<?php echo $s->spesialis_id; ?>">Spesialis <?php echo $s->nama_spesialis; ?></option>
                        <?php } ?>
                      </select>
                      <i class="icon-key"></i></label>
                  </li>
                  <li class="col-sm-6">
                    <label>
                      <input type="text" class="form-control" name="umur" id="umur" placeholder="Umur Anda" required>
                      <i class="icon-calendar"></i> </label>
                  </li>
                  <li class="col-sm-12">
                    <label>
                      <textarea class="form-control" placeholder="Keluhan Penyakit Anda" name="keluhan_penyakit" id="keluhan_penyakit"></textarea>
                    </label>
                  </li>
                  <li class="col-sm-12">
                    <button type="submit" class="btn">Kirim Pendaftaran</button>
                  </li>
                </ul>
              </form>
          <?php
					}
			  ?>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php

	include ('token.php'); 
?>