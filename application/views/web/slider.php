 <!-- Bnr Header -->
  <section class="main-banner">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="<?php echo base_url(); ?>/assets/web/images/slider1.jpg"  alt="slider"  data-bgposition=" center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption sfl tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-120" 
                data-speed="800" 
                data-start="800" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.03" 
                data-endelementdelay="0.4" 
                data-endspeed="300"
                style="z-index: 5; font-size:50px; font-weight:500; color:#fff;  max-width: auto; max-height: auto; white-space: nowrap;">Selamat Datang di Clinic</div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption sfr tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-60" 
                data-speed="800" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.03" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 6; font-size:40px; color:#fff; font-weight:500; white-space: nowrap;">Siaga Medika Purwokerto </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="0" 
                data-speed="800" 
                data-start="1200" 
                data-easing="Power3.easeInOut" 
                data-splitin="none" 
                data-splitout="none" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                style="z-index: 7;  font-size:22px; color:#fff; font-weight:500; max-width: auto; max-height: auto; white-space: nowrap;">Klinik terbaik di purwokerto</div>
          </li>
          
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="<?php echo base_url(); ?>/assets/web/images/slider2.jpg"  alt="slider"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            
            <!-- LAYER NR. 1 -->
            <div class="tp-caption sfl tp-resizeme" 
                data-x="left" data-hoffset="0" 
                data-y="center" data-voffset="-100" 
                data-speed="800" 
                data-start="800" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.03" 
                data-endelementdelay="0.4" 
                data-endspeed="300"
                style="z-index: 5; font-size:40px; font-weight:500; color:#fff;  max-width: auto; max-height: auto; white-space: nowrap;">Pelayanan Terbaik </div>
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption sfr tp-resizeme" 
                data-x="left" data-hoffset="0" 
                data-y="center" data-voffset="-40" 
                data-speed="800" 
                data-start="800" 
                data-easing="Power3.easeInOut" 
                data-splitin="chars" 
                data-splitout="none" 
                data-elementdelay="0.03" 
                data-endelementdelay="0.4" 
                data-endspeed="300"
                style="z-index: 5; font-size:55px; font-weight:500; color:#fff;  max-width: auto; max-height: auto; white-space: nowrap;">Hello, Kami dari klinik Siaga Medika</div>
       </li>
          
          <!-- SLIDE  -->
          <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" > 
            <!-- MAIN IMAGE --> 
            <img src="<?php echo base_url(); ?>/assets/web/images/slider3.jpg"  alt="slider"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"> 
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption sfb tp-resizeme" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-80" 
                data-speed="800" 
                data-start="800" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 6; font-size:40px; color:#fff; font-weight:500; white-space: nowrap;"> Selamat Datang Di Pusat Penelitian Kami </div>
            
            <!-- LAYER NR. 3 -->
            <div class="tp-caption sfb tp-resizeme text-center" 
                data-x="center" data-hoffset="0" 
                data-y="center" data-voffset="-10" 
                data-speed="800" 
                data-start="1000" 
                data-easing="Power3.easeInOut" 
                data-elementdelay="0.1" 
                data-endelementdelay="0.1" 
                data-endspeed="300" 
                data-scrolloffset="0"
                style="z-index: 7; font-size:20px; font-weight:500; line-height:26px; color:#fff; max-width: auto; max-height: auto; white-space: nowrap;">Kami bekerja dengan ramah dan efisien menggunakan Situs terbaru
              teknologi dan berbagi keahlian kami.</div>
          </li>
        </ul>
      </div>
    </div>
  </section>
 