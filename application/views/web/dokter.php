
<!-- Heading -->
      <div class="heading-block">
        <h4></h4>
        <br>
        <h4>Kami Memiliki Team Dokter Profesional</h4></div>
      
      <!-- OUR TEAM -->
      <div class="our-team"> 
        
        <!-- Team  -->
        <div class="team-part">
          <div class="row"> 
            <?php foreach ($dokter as $d) {
            ?>
            <!-- Team Member -->
            <div class="col-sm-4">
              <article> 
                <!-- Team Img -->
                <div class="team-img"> <img class="img-responsive" src="<?php echo base_url();?>file/dokter/<?php echo $d->foto ?>" height="480px" width="370px"> 
                
                <!-- Team Name -->
                <div class="team-name">
                  <h6><?php echo $d->nama_dokter; ?></h6>
                  <p>Spesialis <?php echo $d->spesialis ?></p>
                </div>
              </article>
            </div>
            <?php } ?>
        </div>
          </div>
        </div>
      </div>
    </section>
  </div>
   <?php

  include ('token.php'); 
?>