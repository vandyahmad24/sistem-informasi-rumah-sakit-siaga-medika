  <!-- Footer -->
  <footer class="footer-2">
    <div class="container">
      <div class="row"> 
        
        <!-- Support -->
        <div class="col-sm-4">
          <img src="<?php echo base_url(); ?>/assets/web/images/logo-light.png">
          <div class="about-foot"><br>
            <p>Di bawah kendali Yayasan Siaga Sejahtera yang didirikan di Purwokerto tahun 2003, dan dituangkan dalam bentuk Akta Pendirian Yayasan oleh Notaris Gati Sudardjo, SH pada tanggal 18 Juni 2003 di Purwokerto serta telah mendapatkan pengesahan dari Kementrian Hukum dan HAM pada tanggal 18 Agustus 2010, Klinik Siaga Medika Banyumas merupakan salah satu kegiatan yang masih digeluti hingga saat ini. </p>
          </div>
        </div>
        
        <!-- Latest Blog News -->
        <div class="col-md-3">
          <h6>Berita Terbaru  </h6>
          <div class="latest-post-small"> 
            
            <!-- Blog News -->
            <div class="media">
              <div class="media-left"> <a href="#." class="post-img"><img src="<?php echo base_url(); ?>/assets/web/images/footer-post-img-1.png" alt=""></a> </div>
              <div class="media-body"> <a href="#.">The Hospital Department Patient</a> <span>16/02/17</span> </div>
            </div>
            
            <!-- Blog News -->
            <div class="media">
              <div class="media-left"> <a href="#." class="post-img"><img src="<?php echo base_url(); ?>/assets/web/images/footer-post-img-2.png" alt=""></a> </div>
              <div class="media-body"> <a href="#.">The Hospital Department Patient</a> <span>16/02/17</span> </div>
            </div>
            
            <!-- Blog News -->
            <div class="media">
              <div class="media-left"> <a href="#." class="post-img"><img src="<?php echo base_url(); ?>/assets/web/images/footer-post-img-3.png" alt=""></a> </div>
              <div class="media-body"> <a href="#.">The Department Of Hospital  Patient</a> <span>16/02/17</span> </div>
            </div>
          </div>
        </div>
        
       
        
        <!-- Conatct -->
        <div class="col-sm-3">
          <div class="con-info">
            <h6>Hubungi Kami</h6>
            <?php foreach ($alamat as $k) { ?>
            <p><?php echo $k->content; ?></p>
          <?php } ?>
            <ul>
              <li class="mobile">
               <?php foreach ($kontak as $k) { ?>
                <p><?php echo $k->content; ?></p>
              <?php } ?>
              </li>

              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  
  <!-- Rights -->
  <div class="rights style-2">
    <div class="container">
      <p>
        <?php foreach ($footer as $k) { ?>
                <p><?php echo $k->content; ?></p>
              <?php } ?>
      </p>
    </div>
  </div>
  
  <!-- GO TO TOP  --> 
  <a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a> 
  <!-- GO TO TOP  --> 
</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 
<script src="<?php echo base_url(); ?>/assets/web/js/vendors/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/web/js/vendors/wow.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/web/js/vendors/bootstrap.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/web/js/vendors/own-menu.js"></script> 
<script src="<?php echo base_url(); ?>/assets/web/js/vendors/jquery.sticky.js"></script> 
<script src="<?php echo base_url(); ?>/assets/web/js/vendors/owl.carousel.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/web/js/vendors/jquery.magnific-popup.min.js"></script> 

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/web/rs-plugin/js/jquery.tp.t.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/web/rs-plugin/js/jquery.tp.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/web/js/main.js"></script>
</body>
</html>