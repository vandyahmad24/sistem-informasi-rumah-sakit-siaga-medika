<script type="text/javascript">
	function konfirmasi()
	{
		var check = confirm('Yakin ingin menghapus data dokter ini..?');
		if(check) {
			return true;
			}
			else{
				return false;
				}
		}
</script>

<!-- Traffic sources -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h6 class="panel-title">Selamat Datang, <b><?php echo $this->session->userdata('admin_name');?></b>, Anda login sebagai Administrator</h6>
                            <hr>
							<div class="heading-elements">
								
							</div>
						</div>

						<div class="panel-heading">
							<div class="row">

<a href="<?php echo base_url();?>index.php/administrator/dokter_add" class="btn btn-primary btn-sm"><i class="icon-add"></i> Tambah Data Dokter</a> <br /><br />
<table width="100%" border="1" class="table" bordercolor="#66CCCC" cellspacing="0" cellpadding="0">
  <tr class="bg-blue">
    <td align="center">No.</td>
    <td align="center">Id Spesialis</td>
    <td align="center">Nama Dokter</td>
    <td align="center">Email (Login)</td>
    <td align="center">Spesialis</td>
    <td align="center">Shift Kerja</td>
    <td align="center">Foto Dokter</td>
    <td align="center">Delete</td>
    </tr>
    <?php $no=1; ?>
    <?php foreach ($dokter as $p) { ?>
  <tr>
    <td align="center"><?php echo $no++; ?></td>
    <td align="center"><?php echo $p->spesialis_id; ?> </td>
    <td align="center"><?php echo $p->nama_dokter; ?></td>
    <td><?php echo $p->email_dokter; ?> </td>
    <td align="center"><?php echo $p->spesialis; ?></td>
    <td align="center"><?php echo $p->shift; ?></td>
    <td align="center"><img src="<?php echo base_url();?>file/dokter/<?php echo $p->foto; ?>" width="50" height="55" /></td>
    <td align="center"><a href="<?= base_url();?>index.php/administrator/dokter_delete/<?php echo $p->dokter_id; ?>" onclick="return konfirmasi();"><i class="icon-trash"></i></td></a>
    </tr>
    <?php } ?>
</table>

                                
							</div>
						</div>

						<div class="position-relative" id="traffic-sources"></div>
					</div>
					<!-- /traffic sources -->
