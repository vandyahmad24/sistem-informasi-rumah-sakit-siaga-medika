<script type="text/javascript">
	function konfirmasi()
	{
		var check = confirm('Yakin ingin menghapus data spesialis ini..?');
		if(check) {
			return true;
			}
			else{
				return false;
				}
		}
</script>

<!-- Traffic sources -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h6 class="panel-title">Selamat Datang, <b><?php echo $this->session->userdata('admin_name');?></b>, Anda login sebagai Administrator</h6>
                            <hr>
							<div class="heading-elements">
								
							</div>
						</div>

						<div class="panel-heading">
							<div class="row">

<a href="<?php echo base_url();?>index.php/administrator/spesialis_add" class="btn btn-primary btn-sm"><i class="icon-add"></i> Tambah Data Spesialis</a> <br /><br />
<table width="100%" border="1" class="table" bordercolor="#66CCCC" cellspacing="0" cellpadding="0">
  <tr class="bg-blue">
    <td align="center">No.</td>
    <td align="center">Id Spesialis</td>
    <td align="center">Nama Spesialis</td>
    <td align="center">Akomodasi</td>
    <td align="center">Status</td>
    <td align="center">Update</td>
    <td align="center">Delete</td>
    </tr>
    <?php $no=1; ?>
    <?php foreach ($spesialis as $p) { ?>
  <tr>
    <td align="center"><?php echo $no++; ?></td>
    <td align="center"><?php echo $p->spesialis_id; ?> </td>
    <td align="center"><?php echo $p->nama_spesialis; ?></td>
    <td><?php echo $p->akomodasi; ?> </td>
    <td align="center"><?php
    if($p->aktif ==1) { echo "<span class='label border-left-success label-striped'>Aktif</span>";}
	   else
	   { echo "<span class='label border-left-warning label-striped'>Non-Aktif</span>"; }
	?></td>
    <td align="center"><a href="<?= base_url();?>index.php/administrator/spesialis_edit/<?php echo $p->spesialis_id; ?>"><i class="icon-quill4"></i></a></td>
    <td align="center"><a href="<?= base_url();?>index.php/administrator/spesialis_delete/<?php echo $p->spesialis_id; ?>" onclick="return konfirmasi();"><i class="icon-trash"></i></td></a>
    </tr>
    <?php } ?>
</table>

                                
							</div>
						</div>

						<div class="position-relative" id="traffic-sources"></div>
					</div>
					<!-- /traffic sources -->
