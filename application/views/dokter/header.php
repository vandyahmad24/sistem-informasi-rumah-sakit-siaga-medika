<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $title; ?></title>
<link rel="shortcut icon" href="<?php echo base_url();?>assets/web/images/favicon.ico" type="image/x-icon">
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/backend/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/backend/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/backend/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/backend/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/backend/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/loaders/blockui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/clock.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/ui/nicescroll.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/ui/drilldown.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/pages/dashboard_boxed_full.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/plugins/ui/ripple.min.js"></script>
	
	<!-- /theme JS files -->

</head>

<body onLoad="goforit();" class="layout-boxed navbar-top">

	<!-- Main navbar -->
	<div class="navbar navbar-inverse bg-indigo navbar-fixed-top">
		<div class="navbar-header">
			<a class="navbar-brand" href="index.html"><img src="<?php echo base_url();?>assets/backend/images/logo_light.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li class="<?php if ($title == 'Dashboard Dokter') 
					{
						echo 'active';
					}
				?>">
					<a href="<?php echo base_url(); ?>index.php/dokter">
						<i class="icon-home5"></i> Dashboard 
					</a>

				<li class="<?php if ($title == 'Data Pasien') 
					{
						echo 'active';
					}
				?>">
					<a href="<?php echo base_url(); ?>index.php/dokter/pasien">
						<i class="icon-people"></i> Data Pasien 
					</a>

					
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li >
					<a href="#">
						<i class="icon-people"></i>
						<span class="visible-xs-inline-block position-right">Users</span>
					</a>
					
					
				</li>

				

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url();?>assets/backend/images/dokter.png" alt="">
						<span>
							
							<?php echo $this->session->userdata('nama_dokter');?>
							</span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="<?php echo base_url();?>index.php/dokter/logout"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-right7 position-left"></i> <span class="text-semibold"> Dashboard Dokter</h4>
						</div>

						<div class="heading-elements">
							<img src="<?php echo base_url();?>assets/backend/images/husada.png" alt="">
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>index.php/dokter/"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Dashboard</li>
						</ul>

						<ul class="breadcrumb-elements">
						
							<li class="dropdown">
								<a href="#" class="dropdown-toggle">
									<li><a href="#"><i class="icon-calendar2 position-left"></i> <span id="clock"></span></a></li>
									
									
								</a>

								
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->