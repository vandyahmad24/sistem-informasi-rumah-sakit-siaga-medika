<?php
	
	class Login_dokter extends CI_Controller
	{
		public function index()
			{

				$data['footer']			= $this->Config_model->footer();
				$this->load->view('dokter/login',$data);
			}	
		public function login()
			{
					$email_dokter		= $this->input->post('email_dokter', true);
					$password_dokter	= $this->input->post('password_dokter',true);
					$result				= $this->Dokter_model->dokter_signin($email_dokter,$password_dokter);

					if ($result) {
						$sdata = array();
						$sdata['dokter_id'] = $result->dokter_id;
						$sdata['nama_dokter'] = $result->nama_dokter;
						$this->session->set_userdata($sdata);
						redirect('Dokter');
					}
					else{
						$sdata = array();
						$sdata['pesan_error'] = 'Email atau password salah';
						$this->session->set_userdata($sdata);
						redirect('Login_dokter');
					}

			}
	}
?>