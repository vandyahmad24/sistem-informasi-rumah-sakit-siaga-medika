<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$admin_id = $this->session->userdata('admin_id');
		if ($admin_id == NULL)
		{
			redirect('login_admin');
			}
	}
	
	public function index()
	{
		$data['title']			= 'Dashboard Admin';
		
		$data['dokter']			= $this->Administrator_model->getDokter();
		$data['totaldokter']	= $data['dokter']->num_rows();
		
		$data['pasien0']		= $this->Administrator_model->getPasien0();
		$data['totalpasien0']	= $data['pasien0']->num_rows();
		
		$data['pasien1']		= $this->Administrator_model->getPasien1();
		$data['totalpasien1']	= $data['pasien1']->num_rows();
		
		$this->load->view('admin/head',$data);
		//$this->load->view('admin/head2',$data);
		$this->load->view('admin/dashboard');
	}
	
	
	public function pasien()
	{
		$data['title']			= 'Data Pasien';
		$data['pasien']			= $this->Pasien_model->pasien();
		
		$this->load->view('admin/head',$data);
		$this->load->view('admin/data_pasien');
	}
	
	function pasien_delete($pasien_id)
	{
		$this->Administrator_model->pasien_delete_info($pasien_id);
		redirect('administrator/pasien');
	}
	
	public function dokter()
	{
		$data['title']			= 'Data Dokter';
		$data['dokter']			= $this->Dokter_model->dokter();
		
		$this->load->view('admin/head',$data);
		$this->load->view('admin/data_dokter');
	}
	
	function dokter_add()
	{
		$data['title']			= 'Tambah Data Dokter';
		$data['status']			= 'Add';
		$data['kode']			= '';
		$data['dokter']			= $this->Dokter_model->dokter();

		$data['spesialis']		= $this->Dokter_model->spesialis();

		
		$this->load->view('admin/head',$data);
		$this->load->view('admin/dokter_form');
	}
	
	function dokter_save(){
		if($_POST){
			$kode					= $this->input->post('kode');
			$spesialis_id			= $this->input->post('spesialis_id');
			$nama_dokter			= $this->input->post('nama_dokter');
			$email_dokter			= $this->input->post('email_dokter');
			$password_dokter		= md5($this->input->post('password_dokter'));
			$spesialis				= $this->input->post('spesialis');
			$shift					= $this->input->post('shift');
			$foto					= $this->input->post('foto');
			$status					= $this->input->post('status');
			

					
			if($status == 'Add'){
				if($_FILES['foto']['name'] != ""){
					$config['upload_path']          = 'file/dokter';
                	$config['allowed_types']        = 'jpeg|jpg|png';
                	$config['max_size']             = '2000';
					$config['remove_space']			= true;
					$config['overwrite']			= true;
					$config['encrypt_name']			= false;
					$config['max_width'] 			= '';
					$config['max_height']			= '';
					
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('foto'))
					{
						print_r('Ukuran file terlalu besar. Maksimal 2 MB');
						exit();
						}
					else
					{
						$image = $this->upload->data();
						if($image['file_name'])
						{
							$data['file'] = $image['file_name'];
							}
						$foto = $data['file'];
					}
				}
				
				$data = array(
							  'spesialis_id'	=> $spesialis_id,
							  'nama_dokter'		=> $nama_dokter,
							  'email_dokter'	=> $email_dokter,
							  'password_dokter'	=> $password_dokter,
							  'spesialis'		=> $spesialis,
							  'shift'			=> $shift,
							  'foto'			=> $foto,
							  );
				$this->Administrator_model->insertdata('dokter',$data);
			
				redirect('Administrator/dokter');
			}
			else{
				$this->db->where('dokter_id',$kode);
				$query	= $this->db->get('dokter');
				$row	= $query->row();
				
				unlink(".file/dokter/$row->foto");
				if($_FILES['foto']['name'] != ""){
					$config['upload_path']          = 'file/dokter';
                	$config['allowed_types']        = 'jpeg|jpg|png';
                	$config['max_size']             = '2000';
					$config['remove_space']			= true;
					$config['overwrite']			= true;
					$config['encrypt_name']			= false;
					$config['max_width'] 			= '';
					$config['max_height']			= '';
					
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload('foto'))
					{
						print_r('Ukuran file terlalu besar. Maksimal 2 MB');
						exit();
						}
					else
					{
						$image = $this->upload->data();
						if($image['file_name'])
						{
						$data['file'] = $image['file_name'];
						}
						$foto = $data['file'];
						}
					}
				
				$data = array(
							  'spesialis_id'	=> $spesialis_id,
							  'nama_dokter'		=> $nama_dokter,
							  'email_dokter'	=> $email_dokter,
							  'password_dokter'	=> $password_dokter,
							  'spesialis'		=> $spesialis,
							  'shift'			=> $shift,
							  'foto'			=> $foto,
							 );
				$this->Administrator_model->updatedata('dokter',$data,array('dokter_id' => $kode));
				
				redirect('Administrator/dokter');
				}
		}
		else{
			echo "Halaman tidak ditemukan";
			}
	}
	
	
	function dokter_delete($dokter_id)
	{
		$this->Administrator_model->dokter_delete_info($dokter_id);	
		redirect('Administrator/dokter');
	}
	
	
	public function spesialis()
	{
		$data['title']			= 'Data Spesialis';
		$data['spesialis']		= $this->Administrator_model->spesialis();
		
		$this->load->view('admin/head',$data);
		$this->load->view('admin/data_spesialis');
	}
	
	public function spesialis_add()
	{
		$data['title']			= 'Data Spesialis';
		$data['status']			= 'Add';
		$data['kode']			= '';
		
		$data['spesialis']		= $this->Administrator_model->getSpesialis();
		
		$data['spesialis_id']	= 'spesialis_id';
		$data['nama_spesialis']	= 'nama_spesialis';
		$data['akomodasi']		= 'akomodasi';
		$data['status_aktif']	= 'status';
		
		
		$this->load->view('admin/head',$data);
		$this->load->view('admin/spesialis_form');
	}
	
	function spesialis_save()
	{
		if($_POST){
			$kode				= $this->input->post('kode');
			$akomodasi			= $this->input->post('akomodasi');
			$nama_spesialis		= $this->input->post('nama_spesialis');
			$aktif				= $this->input->post('aktif');
			$status				= $this->input->post('status');
			
			if($status =='Add') {
									$data = array(
												  'nama_spesialis'			=> $nama_spesialis,
												  'akomodasi'				=> $akomodasi,
												  'aktif'					=> $aktif,
												  );
									
									$this->Administrator_model->insertdata('spesialis',$data);
									redirect('administrator/spesialis');
								}
								else
								{
									$data = array(
												  'nama_spesialis'			=> $nama_spesialis,
												  'akomodasi'				=> $akomodasi,
												  'aktif'					=> $aktif,
												  );
									$this->Administrator_model->updatedata('spesialis',$data, array('spesialis_id' => $kode ));
									redirect('administrator/spesialis');
								}
			}
	}
	
	
	
	function spesialis_edit($kode = 0)
	{
		$data_spesialis		=	$this->Administrator_model->getSpesialis("where spesialis_id = '$kode'")->result_array();
		
		$data				= array(
									'title'				=> 'Edit Data Spesialis',
									'kode'				=> $data_spesialis[0]['spesialis_id'],
									'nama_spesialis'	=> $data_spesialis[0]['nama_spesialis'],
									'akomodasi'			=> $data_spesialis[0]['akomodasi'],
									'aktif'				=> $data_spesialis[0]['aktif'],
									'status'			=> 'Edit',
									);
		$this->load->view('admin/head',$data);
		$this->load->view('admin/spesialis_form');
	}
	
	function spesialis_delete($spesialis_id)
	{
		$this->Administrator_model->spesialis_delete_info($spesialis_id);
		redirect('administrator/spesialis');

	}
	
	
	function pesan()
	{
		$data['title']			= 'Kotak Pesan';
		$data['pesan']			= $this->Administrator_model->getPesan();
		
		$this->load->view('admin/head',$data);
		$this->load->view('admin/kotak_pesan');
	}
	
	function pesan_delete($id)
	{
		$this->Administrator_model->pesan_delete_info($id);	
		redirect('Administrator/pesan');
	}
	
	function logout()
	{
		$this->session->unset_userdata('admin_id');	
		$this->session->unset_userdata('admin_name');	
		redirect('login_admin');
	}
	
	
	
	
	
}
