-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Agu 2019 pada 05.16
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simrs`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(30) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`) VALUES
(3, 'administrator', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3'),
(4, 'vandy ahmad', 'vandy@admin.com', 'f26cf562cae7fdeefc3eb3fbccb7e721');

-- --------------------------------------------------------

--
-- Struktur dari tabel `config`
--

CREATE TABLE `config` (
  `id_config` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `config`
--

INSERT INTO `config` (`id_config`, `title`, `content`) VALUES
(1, 'title', 'Klinik Siaga Medika Purwokerto'),
(2, 'deskripsi', 'Klinik Siaga Medika Purwokerto'),
(3, 'keyword', 'Klinik Siaga Medika Purwokerto'),
(4, 'sambutan', '<p>Assalam mu’alaikum wr. wb.</p>Puji syukur rahmad dan karunia Allah SWT sehingga kami dapat menerbitkan website resmi Klinik Siaga Medika Purwokerto sebagai sarana informasi dan komunikasi up-date. <br /><br />Kami berharap, dengan adanya media layanan informasi situs ini semoga dapat mewujudkan hubungan silaturrahmi yang lebih erat lagi, menambah wawasan, mempermudah dan mempercepat proses dalam mendapatkan info yang dibutuhkan.'),
(5, 'direktur', 'Prof. Dr. Bekti Sutraja, M.Si'),
(6, 'alamat', '<p>Jln. HR Boenyamin Gang Sindoro no 16 A<br /> E-mail : humas@siagamedika<br /> Telepon: (0651) 755-3205 Fax : (0651) 755-4229 </p>'),
(7, 'footer', '<p>© 2019 Klinik Siaga Medika Purwokerto - All Rights Reserved</p>'),
(8, 'kontak', '082214688281'),
(9, 'email', 'vandahmad2404@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokter`
--

CREATE TABLE `dokter` (
  `dokter_id` int(11) NOT NULL,
  `spesialis_id` int(11) NOT NULL,
  `nama_dokter` varchar(30) NOT NULL,
  `email_dokter` varchar(30) NOT NULL,
  `password_dokter` varchar(64) NOT NULL,
  `spesialis` char(30) NOT NULL,
  `shift` varchar(11) NOT NULL,
  `foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `dokter`
--

INSERT INTO `dokter` (`dokter_id`, `spesialis_id`, `nama_dokter`, `email_dokter`, `password_dokter`, `spesialis`, `shift`, `foto`) VALUES
(1, 2, 'dr. Nahda Rafanda', 'nahda@gmail.com', '9bc7b9c8a5508952a7788a667bee39e0', 'Bedah', '09-17', 'nahda.jpg'),
(2, 5, 'dr. Nun Aliffa Afyuni', 'nun@gmail.com', 'a7008e5a8a8ce8819f27fb260e330d0e', 'Jantung', '09-17', 'aliffa.jpg'),
(20, 2, 'Vandy ahmad', 'vandy@gmail.com', 'f26cf562cae7fdeefc3eb3fbccb7e721', 'Bedah', '9-12', 'vandy.jpg\r\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak`
--

CREATE TABLE `kontak` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `sex` varchar(15) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `kontak`
--

INSERT INTO `kontak` (`id`, `nama`, `sex`, `hp`, `email`, `message`) VALUES
(4, 'Fahri Wijaya', 'Laki-Laki', '082214688281', 'fahri.wijaya@gmail.com', 'Terimakasih atas hadirnya fasilitas cek hasil pemeriksaan secara online. Sangat membantu sekali'),
(5, 'Sarifah', 'Perempuan', '082214688281', 'sarifah.imuet2000@gmail.com', 'Sukses selalu buat rumah sakit ini yang menyajikan informasi akurat dan terpercaya'),
(6, 'Yoga Pratama', 'Laki-Laki', '082214688281', 'oga.ndut2000@gmail.com', 'Rumah sakit yang mengikuti perkembangan teknologi, saluuuttt...'),
(7, 'Fatih Fatahillah', 'Laki-Laki', '082214688281', 'fatih.fatahillah2017@gmail.com', 'web nya keren ditambah sistem cek token yang mantap.'),
(8, 'hanif', '', '131332', 'dimas@gmail.com', 'Masuk ga'),
(9, 'hanif', '', '131332', 'dimas@gmail.com', 'Masuk ga'),
(10, '3', '', '123', '3@gmail.com', 'Masukan Jenis\r\n'),
(11, '3', '', '123', '3@gmail.com', 'Masukan Jenis\r\n'),
(12, '3', '', '123', '3@gmail.com', 'Masukan Jenis\r\n'),
(13, 'hanif', '', '123', 'vandyahmad2404@gmail.com', 'rm'),
(14, '77', '', '71237', '7@gmail.com', 'dsadasd'),
(15, 'hanif', '', '123', 'vandyahmad2404@gmail.com', 'kmkm');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `pasien_id` int(11) NOT NULL,
  `spesialis_id` int(11) NOT NULL,
  `perawatan_id` int(10) NOT NULL,
  `nama_pasien` varchar(30) DEFAULT NULL,
  `kontak` varchar(20) NOT NULL,
  `umur` int(3) NOT NULL,
  `sex` char(1) NOT NULL,
  `email_pasien` varchar(30) NOT NULL,
  `keluhan_penyakit` text NOT NULL,
  `progress` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`pasien_id`, `spesialis_id`, `perawatan_id`, `nama_pasien`, `kontak`, `umur`, `sex`, `email_pasien`, `keluhan_penyakit`, `progress`) VALUES
(35, 4, 1908071131, 'Vandy Ahmad', '09731264126', 20, 'P', 'vandyahmad2404@gmail.com', 'Burem saat melihat', 1),
(36, 1, 1908071121, 'dasd', '98', 213, 'P', 'dsad@gmail.com', 'diajdsk\r\n', 1),
(37, 3, 1908071151, 'lutfi', '0821313', 12, 'P', 'lutfiania@gmail.com', 'ompong', 1),
(38, 1, 1908071132, 'dsadkjak', '231931398', 0, 'P', 'jdksjadkj@gmail.com', 'jdskadja', 0),
(40, 1, 1908080148, 'irhamul huda', '0239138412', 16, 'P', 'irhamulhuda@gmail.com', 'Batuk Pilek', 0),
(41, 1, 1908080835, 'Vandy Ahmad', '23132', 16, 'P', 'misry_arrazy@gmail.com', 'Diare', 0),
(42, 4, 1908090734, 'vandy ahmad', 'd32193', 17, 'P', 'vandyahmad@gmail.com', 'Sakit mata', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `perawatan`
--

CREATE TABLE `perawatan` (
  `perawatan_id` int(10) NOT NULL,
  `dokter_id` int(11) NOT NULL,
  `detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `perawatan`
--

INSERT INTO `perawatan` (`perawatan_id`, `dokter_id`, `detail`) VALUES
(1908071121, 20, 'Ojo gendeng mas'),
(1908071131, 2, 'Segera berobat'),
(1908071151, 20, 'tambal mba'),
(1908090936, 20, 'aman');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spesialis`
--

CREATE TABLE `spesialis` (
  `spesialis_id` int(11) NOT NULL,
  `nama_spesialis` varchar(30) NOT NULL,
  `akomodasi` int(11) NOT NULL,
  `aktif` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `spesialis`
--

INSERT INTO `spesialis` (`spesialis_id`, `nama_spesialis`, `akomodasi`, `aktif`) VALUES
(1, 'Anak', 30, 1),
(2, 'Bedah', 30, 1),
(3, 'Gigi', 30, 1),
(4, 'Mata', 30, 1),
(5, 'Jantung', 30, 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indeks untuk tabel `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id_config`);

--
-- Indeks untuk tabel `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`dokter_id`);

--
-- Indeks untuk tabel `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`pasien_id`);

--
-- Indeks untuk tabel `perawatan`
--
ALTER TABLE `perawatan`
  ADD PRIMARY KEY (`perawatan_id`);

--
-- Indeks untuk tabel `spesialis`
--
ALTER TABLE `spesialis`
  ADD PRIMARY KEY (`spesialis_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `config`
--
ALTER TABLE `config`
  MODIFY `id_config` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `dokter`
--
ALTER TABLE `dokter`
  MODIFY `dokter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `pasien`
--
ALTER TABLE `pasien`
  MODIFY `pasien_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `perawatan`
--
ALTER TABLE `perawatan`
  MODIFY `perawatan_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1908090937;

--
-- AUTO_INCREMENT untuk tabel `spesialis`
--
ALTER TABLE `spesialis`
  MODIFY `spesialis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
